<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Address;
use EsperoSoft\Faker\Faker;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class BlogFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = new Faker();
        $users = [];
        for ($i=0; $i < 100 ; $i++) { 
            $user = (new User())->setFullName($faker->full_name())
            ->setEmail($faker->email())
            ->setPassword(sha1('admin'))
            ->setCreatedAt($faker->dateTimeImmutable());

            $address = (new Address())->setStreet($faker->streetAddress())
            ->setCodePostal($faker->codePostal())
            ->setCity($faker->city())
            ->setCountry($faker->country())
            ->setCreatedAt($faker->dateTimeImmutable());
            
            $user->addAddress($address);
            $users[] = $user;
            $manager->persist($address);
            $manager->persist($user);
        }

        $manager->flush();
    }
}
